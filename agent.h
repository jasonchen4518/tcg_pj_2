#pragma once
#include <string>
#include <random>
#include <sstream>
#include <map>
#include <type_traits>
#include <algorithm>
#include "board.h"
#include "action.h"
#include "weight.h"
#include <fstream>

class agent {
public:
	agent(const std::string& args = "") {
		std::stringstream ss("name=unknown role=unknown " + args);
		for (std::string pair; ss >> pair; ) {
			std::string key = pair.substr(0, pair.find('='));
			std::string value = pair.substr(pair.find('=') + 1);
			meta[key] = { value };
		}
	}
	virtual ~agent() {}
	virtual void open_episode(const std::string& flag = "") {}
	virtual void close_episode(const std::string& flag = "") {}
	virtual action take_action(const board& b) { return action(); }
	virtual bool check_for_win(const board& b) { return false; }

public:
	virtual std::string property(const std::string& key) const { return meta.at(key); }
	virtual void notify(const std::string& msg) { meta[msg.substr(0, msg.find('='))] = { msg.substr(msg.find('=') + 1) }; }
	virtual std::string name() const { return property("name"); }
	virtual std::string role() const { return property("role"); }

protected:
	typedef std::string key;
	struct value {
		std::string value;
		operator std::string() const { return value; }
		template<typename numeric, typename = typename std::enable_if<std::is_arithmetic<numeric>::value, numeric>::type>
		operator numeric() const { return numeric(std::stod(value)); }
	};
	std::map<key, value> meta;
};

class random_agent : public agent {
public:
	random_agent(const std::string& args = "") : agent(args) {
		if (meta.find("seed") != meta.end())
			engine.seed(int(meta["seed"]));
	}
	virtual ~random_agent() {}

protected:
	std::default_random_engine engine;
};

/**
 * base agent for agents with weight table
 */
class weight_agent : public agent {
public:
	weight_agent(const std::string& args = "") : agent(args) , alpha(0.1f) {
		if(meta.find("init") != meta.end()) //pass init=... to initialize the weight
			init_weights(meta["init"]);
		if(meta.find("load") != meta.end()) //pass load=... to load from a specific file
			load_weights(meta["load"]);
		if(meta.find("alpha") != meta.end())
			alpha = float(meta["alpha"]);
	}
	virtual ~weight_agent() {
		if(meta.find("save") != meta.end()) //pass save=... to save to a specific file
			save_weights(meta["save"]);
	}

protected:
	virtual void init_weights(const std::string& info) {
		net.emplace_back(50625); //create an empty weight table with size 50625
		net.emplace_back(50625); //create an empty weight table with size 50625
		// now net.size() == 2; net[0].size() == 50625; net[1].size() == 50625
	}
	virtual void load_weights(const std::string& path) {
		std::ifstream in(path, std::ios::in | std::ios::binary);
		if (!in.is_open()) std::exit(-1);
		uint32_t size;
		in.read(reinterpret_cast<char*>(&size), sizeof(size));
		net.resize(size);
		for (weight& w : net) in >> w;
		in.close();
	}
	virtual void save_weights(const std::string& path) {
		std::ofstream out(path, std::ios::out | std::ios::binary | std::ios::trunc);
		if (!out.is_open()) std::exit(-1);
		uint32_t size = net.size();
		out.write(reinterpret_cast<char*>(&size), sizeof(size));
		for (weight& w : net) out << w;
		out.close();
	}

protected:
	std::vector<weight> net;
	float alpha;
};

/**
 * base agent for agents with a learning rate
 */
class learning_agent : public agent {
public:
	learning_agent(const std::string& args = "") : agent(args), alpha(0.1f) {
		if(meta.find("alpha") != meta.end())
			alpha = float(meta["alpha"]);
	}
	virtual ~learning_agent() {}

protected:
	float alpha;
};

/**
 * random environment
 * add a new random tile to an empty cell
 * 2-tile: 90%
 * 4-tile: 10%
 */
class rndenv : public random_agent {
public:
	rndenv(const std::string& args = "") : random_agent("name=random role=environment " + args),
		space({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }), bag({1, 2, 3}) {
		std::shuffle(bag.begin(), bag.end(), engine);
	}

	virtual action take_action(const board& after) {
		std::shuffle(space.begin(), space.end(), engine);
		for (int pos : space) {
			board::cell tile;
			if(after.initcount < 9)
			{
				if (after(pos) != 0) continue;
			}
			else{
				//add slide direction here
				if(after(pos) != 0) continue;
				if(after.direction == 0){
					if(pos != 12 && pos != 13 && pos != 14 && pos != 15) continue;
				}
				else if(after.direction == 1){
					if(pos != 0 && pos != 1 && pos != 2 && pos != 3) continue;
				}
				else if(after.direction == 2){
					if(pos != 3 && pos != 7 && pos != 11 && pos != 15) continue;
				}
				else if(after.direction == 3){
					if(pos != 0 && pos != 4 && pos != 8 && pos != 12) continue;
				}
			}
			tile = bag[after.bagcount];
			if(after.bagcount == 2){
				std::shuffle(bag.begin(), bag.end(), engine);
			}
			return action::place(pos, tile);
		}
		return action();
	}

private:
	std::array<int, 16> space;
	std::array<int, 3> bag;
};

/**
 * dummy player
 * select a legal action randomly
 */
class player : public weight_agent {
public:
	player(const std::string& args = "") : weight_agent("name=dummy role=player " + args),
		opcode({ 0, 1, 2, 3 }) { state = 0; pre_v = 0;}

	virtual action take_action(const board& before) {
		max = -10000000;
		for (int op : opcode) {
			board evalu_b = before;
			value = 0;
		//	pre_v = 0;
			board::reward reward = evalu_b.slide(op);
			if(reward != -1){
				feature[0] = net[0][evalu_b(0)*15*15*15+evalu_b(1)*15*15+evalu_b(2)*15+evalu_b(3)];
				feature[1] = net[0][evalu_b(4)*15*15*15+evalu_b(5)*15*15+evalu_b(6)*15+evalu_b(7)];
				feature[2] = net[0][evalu_b(8)*15*15*15+evalu_b(9)*15*15+evalu_b(10)*15+evalu_b(11)];
				feature[3] = net[0][evalu_b(12)*15*15*15+evalu_b(13)*15*15+evalu_b(14)*15+evalu_b(15)];
				feature[4] = net[0][evalu_b(0)*15*15*15+evalu_b(4)*15*15+evalu_b(8)*15+evalu_b(12)];
				feature[5] = net[0][evalu_b(1)*15*15*15+evalu_b(5)*15*15+evalu_b(9)*15+evalu_b(13)];
				feature[6] = net[0][evalu_b(2)*15*15*15+evalu_b(6)*15*15+evalu_b(10)*15+evalu_b(14)];
				feature[7] = net[0][evalu_b(3)*15*15*15+evalu_b(7)*15*15+evalu_b(11)*15+evalu_b(15)];
				for(float f : feature) value = value + f;
				value = value + reward;
			
				if(value > max) {
					max = value;
					next_a = op;
			//		pre_v = value - reward;
				}
			}
		}
		if(max != -10000000) {
			//learning
			board tmp_b = before;
			board::reward reward = tmp_b.slide(next_a);
			//std::cout<<"state: "<<state<<std::endl;
			if(state >= 1) {
				//std::cout<<"learning~~~\n";
				float delta = alpha*(max - pre_v);
	/*			std::cout<<"delta: "<<delta<<std::endl;
				std::cout<<"max: "<<max<<std::endl;
				std::cout<<"pre_v: "<<pre_v<<std::endl;
				//std::cout<<"alpha: "<<alpha<<std::endl;
				std::cout<<"reward: "<<reward<<std::endl;*/
				net[0][pre_b(0)*15*15*15+pre_b(1)*15*15+pre_b(2)*15+pre_b(3)] += (delta / 8);
				net[0][pre_b(4)*15*15*15+pre_b(5)*15*15+pre_b(6)*15+pre_b(7)] += (delta / 8);
				net[0][pre_b(8)*15*15*15+pre_b(9)*15*15+pre_b(10)*15+pre_b(11)] += (delta / 8);
				net[0][pre_b(12)*15*15*15+pre_b(13)*15*15+pre_b(14)*15+pre_b(15)] += (delta / 8);
				net[0][pre_b(0)*15*15*15+pre_b(4)*15*15+pre_b(8)*15+pre_b(12)] += (delta / 8);
				net[0][pre_b(1)*15*15*15+pre_b(5)*15*15+pre_b(9)*15+pre_b(13)] += (delta / 8);
				net[0][pre_b(2)*15*15*15+pre_b(6)*15*15+pre_b(10)*15+pre_b(14)] += (delta / 8);
				net[0][pre_b(3)*15*15*15+pre_b(7)*15*15+pre_b(11)*15+pre_b(15)] += (delta / 8);
			}
			pre_v = max - reward;
			pre_b = tmp_b;
	//		pre_b = before;
	//		pre_b.slide(next_a);
			state++;
			return action::slide(next_a);
		}
		else {
			float delta = alpha*(0 - pre_v);
			net[0][pre_b(0)*15*15*15+pre_b(1)*15*15+pre_b(2)*15+pre_b(3)] += (delta / 8);
			net[0][pre_b(4)*15*15*15+pre_b(5)*15*15+pre_b(6)*15+pre_b(7)] += (delta / 8);
			net[0][pre_b(8)*15*15*15+pre_b(9)*15*15+pre_b(10)*15+pre_b(11)] += (delta / 8);
			net[0][pre_b(12)*15*15*15+pre_b(13)*15*15+pre_b(14)*15+pre_b(15)] += (delta / 8);
			net[0][pre_b(0)*15*15*15+pre_b(4)*15*15+pre_b(8)*15+pre_b(12)] += (delta / 8);
			net[0][pre_b(1)*15*15*15+pre_b(5)*15*15+pre_b(9)*15+pre_b(13)] += (delta / 8);
			net[0][pre_b(2)*15*15*15+pre_b(6)*15*15+pre_b(10)*15+pre_b(14)] += (delta / 8);
			net[0][pre_b(3)*15*15*15+pre_b(7)*15*15+pre_b(11)*15+pre_b(15)] += (delta / 8);
			return action();
		}
	}

private:
	std::array<int, 4> opcode;
	std::array<float, 8> feature;
	int next_a;
	float value;
	float max;
	float pre_v;
	board pre_b;
	int state;
};
